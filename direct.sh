#!/bin/sh
vmname=test
memorystamp=2048M

# create a tap interface
#   ifconfig tap1 create
# add to bridge
#   ifconfig bridge0 addm tap1

# kill (if any)

bhyvectl --vm=${vmname} --destroy

list_sda () {

	hddisk="./"`ls -1 -t direct*.sda | head -1`
}

hddisk=${1}
[ -z ${hddisk} ] && list_sda 

echo "building map file for grub"
echo "(hd0) "${hddisk} > direct.map

grub-bhyve -m direct.map -r hd0,msdos1 -M ${memorystamp} ${vmname}

# at prompt, type
#   linux (hd0,msdos1)/vmlinuz root=/dev/vda2
#   boot

echo "starting linux on root=" ${hddisk}
bhyve -A -H -P -s 0:0,hostbridge \
-s 1:0,lpc \
-s 2:0,virtio-net,tap1 \
-s 3:0,virtio-blk,${hddisk} \
-l com1,stdio \
-c 2 -m ${memorystamp} ${vmname}

bhyvectl --vm=${vmname} --destroy
